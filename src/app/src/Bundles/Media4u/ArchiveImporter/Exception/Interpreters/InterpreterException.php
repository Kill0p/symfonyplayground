<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Exception\Interpreters;

use Exception;

class InterpreterException extends Exception
{

}
