<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Exception;

class ArchiveImporterInvalidArgumentException extends \InvalidArgumentException
{

}
