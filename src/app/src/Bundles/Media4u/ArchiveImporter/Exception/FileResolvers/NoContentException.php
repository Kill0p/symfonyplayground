<?php
declare(strict_types=1);
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 16.01.2020
 */
namespace Media4u\ArchiveImporter\Exception\FileResolvers;

class NoContentException extends FileResolverException
{

}
