<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

use Media4u\ArchiveImporter\Interpreters\AdapterInterface as InterpretersAdapterInterface;
use Media4u\ArchiveImporter\Resolvers\FileResolverInterface;
use Media4u\ArchiveImporter\Segregators\ZippedPictures;
use Media4u\ArchiveImporter\Segregators\ZippedPicturesPaths;

class ManagerFactory
{
    /** @var FileResolverInterface */
    private $fileResolver;
    /** @var InterpretersAdapterInterface */
    private $interpretersAdapter;
    /** @var string */
    private $projectRootDirectoryName;

    public function __construct(
        FileResolverInterface $fileResolver,
        InterpretersAdapterInterface $interpretersAdapter,
        string $projectRootDirectoryName
    ) {
        $this->fileResolver = $fileResolver;
        $this->interpretersAdapter = $interpretersAdapter;
        $this->projectRootDirectoryName = $projectRootDirectoryName;
    }

    public function create(string $picturesImportRelativePath,
                           string $archiveTargetRelativePath,
                           string $picturesTargetRelativePath,
                           string $archiveFileUploadPath): Manager
    {
        $zippedPicturePaths = new ZippedPicturesPaths(
            $picturesImportRelativePath,
            $archiveTargetRelativePath,
            $picturesTargetRelativePath,
            $this->projectRootDirectoryName,
            $archiveFileUploadPath
        );

        $segregator = new ZippedPictures($zippedPicturePaths, $this->fileResolver);
        $failures = new Failures();
        $archiveFileSets = new ArchiveFilesSets();

        return new Manager(
            $archiveFileUploadPath,
            $this->fileResolver,
            $segregator,
            $this->interpretersAdapter,
            $failures,
            $archiveFileSets
        );
    }
}
