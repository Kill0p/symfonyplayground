<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

class Failures extends \ArrayIterator implements FailuresInterface
{
    public function __construct(Failure ...$failures)
    {
        parent::__construct($failures);
    }

    public function offsetGet($offset): Failure
    {
        return parent::offsetGet($offset);
    }

    public function current(): Failure
    {
        return parent::current();
    }

    public function add(\Exception $e = null): Failure
    {
        $failure = new Failure($e);
        $this[] = $failure;

        return $failure;
    }
}
