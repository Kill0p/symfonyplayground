<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

interface ArchiveFilesSetsInterface extends \Countable, \ArrayAccess
{

    public function initNewSet(string $key, ArchiveFileInterface $archiveFile): ArchiveFilesInterface;
}
