<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

use Media4u\ArchiveImporter\Exception\ArchiveImporterInvalidArgumentException;

class ArchiveFilesSets extends \ArrayIterator implements ArchiveFilesSetsInterface
{

    public function __construct(ArchiveFilesInterface ...$archiveFileSets)
    {
        parent::__construct($archiveFileSets);
    }

    public function offsetGet($index): ArchiveFilesInterface
    {
        return parent::offsetGet($index);
    }

    public function offsetSet($index, $value)
    {
        if (!$value instanceof ArchiveFilesInterface) {
            throw new ArchiveImporterInvalidArgumentException('Value must implements '.ArchiveFilesInterface::class);
        }

        parent::offsetSet($index, $value);
    }

    public function current(): ArchiveFilesInterface
    {
        return parent::current();
    }

    public function initNewSet(string $key, ArchiveFileInterface $archiveFile): ArchiveFilesInterface
    {
        $archiveFiles = new ArchiveFiles([$archiveFile]);
        $this[ $key ] = $archiveFiles;

        return $archiveFiles;
    }
}
