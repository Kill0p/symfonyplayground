<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

class Failure implements \JsonSerializable
{
    /** @var string */
    private $message;
    /** @var array */
    private $customData = [];
    /** @var \Exception */
    private $exception;
    /** @var string */
    private $identityValue;

    public function __construct(\Exception $exception = null)
    {
        $this->exception = $exception;
    }

    public function getFormattedExceptionMessages(): array
    {
        $messages = [];
        if (!$this->exception instanceof \Exception) {

            return $messages;
        }
        $e = $this->exception;

        do {
            $messages[] = $e->getMessage();
        } while ($e = $this->exception->getPrevious());

        return $messages;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function setCustomData(array $customData): void
    {
        $this->customData = $customData;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getCustomData(): array
    {
        return $this->customData;
    }

    public function getException(): \Exception
    {
        return $this->exception;
    }

    public function getIdentityValue(): ?string
    {
        return $this->identityValue;
    }

    public function setIdentityValue(string $identityValue): void
    {
        $this->identityValue = $identityValue;
    }

    public function jsonSerialize()
    {
        return [
            'message' => $this->getMessage(),
            'customData' => $this->getCustomData(),
            'exception_messages' => $this->getFormattedExceptionMessages(),
            'identity_value' => $this->getIdentityValue()
        ];
    }
}
