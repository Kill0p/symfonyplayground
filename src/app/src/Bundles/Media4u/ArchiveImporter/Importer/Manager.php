<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

use Media4u\ArchiveImporter\Exception\FileResolvers\FileResolverException;
use Media4u\ArchiveImporter\Exception\FileResolvers\NoContentException;
use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Exception\ManagerException;
use Media4u\ArchiveImporter\Exception\Segregators\SegregatorException;
use Media4u\ArchiveImporter\Resolvers\FileResolverInterface;
use Media4u\ArchiveImporter\Interpreters\AdapterInterface as InterpretersAdapterInterface;
use Media4u\ArchiveImporter\Segregators\SegregatorInterface;

class Manager
{
    /** @var ArchiveFilesInterface */
    private $archiveFiles;
    /** @var FileResolverInterface */
    private $fileResolver;
    /** @var string */
    private $archiveFileUploadPath;
    /** @var SegregatorInterface */
    private $segregator;
    /** @var InterpretersAdapterInterface */
    private $interpretersAdapter;
    /** @var FailuresInterface */
    private $failures;
    /** @var ArchiveFilesSetsInterface */
    private $archiveFileSets;

    public function __construct(
        string $archiveFileUploadPath,
        FileResolverInterface $fileResolver,
        SegregatorInterface $segregator,
        InterpretersAdapterInterface $interpretersAdapter,
        FailuresInterface $failures,
        ArchiveFilesSetsInterface $archiveFileSets
    ) {
        $this->archiveFileUploadPath = $archiveFileUploadPath;
        $this->fileResolver = $fileResolver;
        $this->segregator = $segregator;
        $this->interpretersAdapter = $interpretersAdapter;
        $this->failures = $failures;
        $this->archiveFileSets = $archiveFileSets;
    }

    /**
     * @throws InterpreterException
     * @throws NoContentException
     * @throws SegregatorException
     * @throws FileResolverException
     * @throws ManagerException
     */
    public function import(): void
    {
        $this->archiveFiles = $this->fileResolver->getListOfFilesFromArchive($this->archiveFileUploadPath);

        if ($this->archiveFiles->count() === 0) {

            throw new ManagerException('No archive files was generated from archive file');
        }

        $this->archiveFiles->setSavePathDirectoryNameForFiles($this->segregator->getUnpackDestinationDirectoryName());

        $this->importFiles();
        $this->handleArchiveAfterImport();
        $this->handleArchiveFileSets();
    }

    public function getFailures(): FailuresInterface
    {
        return $this->failures;
    }

    /**
     * @throws FileResolverException
     */
    private function importFiles(): void
    {
        /** @var ArchiveFileInterface $file */
        foreach ($this->archiveFiles as $file) {
            $this->interpretersAdapter->getSaveFullPath($file);
            $this->fileResolver->copyFromArchiveToFile($this->archiveFileUploadPath, $file);
        }
    }

    /**
     * @throws SegregatorException
     * @throws NoContentException
     */
    private function handleArchiveAfterImport(): void
    {
        $this->segregator->handleArchive();
    }

    /**
     * @throws InterpreterException
     */
    private function handleArchiveFileSets(): void
    {
        $this->interpretersAdapter->resolveIdentityKeysForArchiveFiles($this->archiveFiles);
        $this->assignArchiveFilesToArchiveSets();

        /**
         * @var string $identityValue
         * @var ArchiveFilesInterface $archiveFiles
         */
        foreach ($this->archiveFileSets as $identityValue => $archiveFiles) {
            $targetDirectoryNameForArchiveFile = $this->interpretersAdapter->getTargetDirectoryNameForArchiveFile($identityValue);
            $arrangedPicturePaths = $this->interpretersAdapter->arrangeArchiveFiles($archiveFiles);

            try {
                $this->fileResolver->handlePictures($targetDirectoryNameForArchiveFile, $arrangedPicturePaths);
            } catch (\Exception $e) {
                $failure = $this->failures->add($e);
                $failure->setIdentityValue($identityValue);
                $failure->setMessage('Error when handling archive pictures');
                $failure->setCustomData(['archive_file_picture_paths' => json_encode($archiveFiles)]);
            }
        }
    }

    private function assignArchiveFilesToArchiveSets(): void
    {
        /** @var ArchiveFileInterface $file */
        foreach ($this->archiveFiles as $file) {

            $identityValue = $file->getIdentityValue();
            if ($identityValue === null) {
                $failure = $this->failures->add();
                $failure->setCustomData(['original_picture_basename' => $file->getBasename()]);
                $failure->setMessage('Could not get identityValue for file in archive');

                continue;
            }

            if ($this->archiveFileSets->offsetExists($identityValue)) {
                $archiveSet = $this->archiveFileSets[ $identityValue ];
                $archiveSet[] = $file;
                $this->archiveFileSets[ $identityValue ] = $archiveSet;
            } else {
                $this->archiveFileSets->initNewSet($identityValue, $file);
            }
        }
    }
}
