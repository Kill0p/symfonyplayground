<?php

namespace Media4u\ArchiveImporter\Importer;

interface FailuresInterface extends \Countable, \ArrayAccess
{

    public function add(\Exception $e = null): Failure;
}
