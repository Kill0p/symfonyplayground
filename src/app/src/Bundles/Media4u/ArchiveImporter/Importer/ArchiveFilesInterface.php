<?php

namespace Media4u\ArchiveImporter\Importer;

interface ArchiveFilesInterface extends \ArrayAccess, \Countable, \JsonSerializable
{

    public function setSavePathDirectoryNameForFiles(string $directoryName): void;

    public function validateIdentityKeyValueSet(): ArchiveFilesInterface;
}
