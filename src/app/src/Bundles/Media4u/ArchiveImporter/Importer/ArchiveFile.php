<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

class ArchiveFile implements ArchiveFileInterface
{
    /** @var string */
    private $basename;
    /** @var string */
    private $extension;
    /** @var string */
    private $fullPath;
    /** @var string */
    private $savePathDirectoryName;
    /** @var string */
    private $saveFullPath;
    /** @var string */
    private $formattedBaseName;
    /** @var string */
    private $identityKeyValue;

    public function __construct(string $basename, string $extension, string $fullPath)
    {
        $this->basename = $basename;
        $this->extension = $extension;
        $this->fullPath = $fullPath;
    }

    public function getBasename(): string
    {
        return $this->basename;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getFullPath(): string
    {
        return $this->fullPath;
    }

    public function getSavePathDirectoryName(): string
    {
        return $this->savePathDirectoryName;
    }

    public function getSaveFullPath(): ?string
    {
        return $this->saveFullPath;
    }

    public function setSaveFullPath(string $saveFullPath): void
    {
        $this->saveFullPath = $saveFullPath;
    }

    public function getFormattedBaseName(): ?string
    {
        return $this->formattedBaseName;
    }

    public function setFormattedBaseName(string $formattedBaseName): void
    {
        $this->formattedBaseName = $formattedBaseName;
    }

    public function getIdentityValue(): ?string
    {
        return $this->identityKeyValue;
    }

    public function setIdentityKeyValue(?string $identityKeyValue): void
    {
        $this->identityKeyValue = $identityKeyValue;
    }

    public function setSavePathDirectoryName(string $savePathDirectoryName): void
    {
        $this->savePathDirectoryName = $savePathDirectoryName;
    }

    public function jsonSerialize(): array
    {
        return [
            'basename' => $this->getBasename(),
            'full_path' => $this->getFullPath(),
            'save_full_path' => $this->getSaveFullPath(),
            'save_path_directory_name' => $this->getSavePathDirectoryName(),
            'formatted_base_name' => $this->getFormattedBaseName(),
            'identity_value' => $this->getIdentityValue()
        ];
    }
}
