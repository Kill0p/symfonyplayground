<?php

namespace Media4u\ArchiveImporter\Importer;

use JsonSerializable;

interface ArchiveFileInterface extends JsonSerializable
{
    public function getBasename(): string;

    public function getFullPath(): string;

    public function getSavePathDirectoryName(): string;

    public function getExtension(): string;

    public function getSaveFullPath(): ?string;

    public function setFormattedBaseName(string $formattedBaseName): void;

    public function getFormattedBaseName(): ?string;

    public function setSaveFullPath(string $saveFullPath): void;

    public function setIdentityKeyValue(?string $identityKeyValue): void;

    public function setSavePathDirectoryName(string $savePathDirectoryName): void;

    public function getIdentityValue(): ?string;
}