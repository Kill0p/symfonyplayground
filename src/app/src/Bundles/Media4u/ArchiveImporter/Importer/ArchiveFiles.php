<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Importer;

use Media4u\ArchiveImporter\Exception\ArchiveImporterInvalidArgumentException;

class ArchiveFiles extends \ArrayIterator implements ArchiveFilesInterface
{
    public function __construct(ArchiveFileInterface ...$archiveFiles)
    {
        parent::__construct($archiveFiles);
    }

    public function offsetGet($index): ArchiveFileInterface
    {
        return parent::offsetGet($index);
    }

    public function offsetSet($index, $value): void
    {
        if (!$value instanceof ArchiveFileInterface) {

            throw new ArchiveImporterInvalidArgumentException('Value must implements '.ArchiveFileInterface::class);
        }

        parent::offsetSet($index, $value);
    }

    public function current(): ArchiveFileInterface
    {
        return parent::current();
    }

    public function setSavePathDirectoryNameForFiles(string $directoryName): void
    {
        foreach ($this as $file) {
            $file->setSavePathDirectoryName($directoryName);
        }
    }

    public function validateIdentityKeyValueSet(): ArchiveFilesInterface
    {
        $emptyIdentityKeyValueArchiveFiles = new static();

        /** @var ArchiveFileInterface $file */
        foreach ($this as $file) {
            if (empty($file->getIdentityValue())) {
                $emptyIdentityKeyValueArchiveFiles[] = $file;
            }
        }

        return $emptyIdentityKeyValueArchiveFiles;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $serialized = [];
        /** @var ArchiveFileInterface $file */
        foreach ($this as $file) {
            $serialized[] = json_encode($file);
        }

        return $serialized;
    }
}
