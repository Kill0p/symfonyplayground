<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Resolvers;

use Media4u\ArchiveImporter\Exception\FileResolvers\FileResolverException;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

interface FileResolverInterface
{
    /**
     * @throws \Media4u\ArchiveImporter\Exception\FileResolvers\NoContentException
     * @throws \Exception
     */
    public function listContent(string $directoryName): \Media4u\ArchiveImporter\Segregators\FilesInterface;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function deleteDirectory(string $directoryName): void;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function deleteFile(string $fullPath): void;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function copyFile(string $fromFullPath, string $toFullPath): void;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function moveArchive(string $archiveFileUploadPath, string $getNewArchiveFileName, string $newArchiveFileFullPath): void;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function getListOfFilesFromArchive(string $fullPathToArchive): ArchiveFilesInterface;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function copyFromArchiveToFile(string $archiveFileUploadPath, ArchiveFileInterface $file): void;

    /**
     * @throws FileResolverException
     * @throws \Exception
     */
    public function handlePictures(string $targetDirectoryNameForArchiveFile, array $arrangedPicturePaths): void;
}
