<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Resolvers;

use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

interface IdentityKeyResolverInterface
{
    public function acquireIdentityValuesForFiles(ArchiveFilesInterface $archiveFiles): void;
}
