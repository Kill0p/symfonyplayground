<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

class ProductNameSet implements ProductNameSetInterface
{
    use ProductNameTrait;

    private const MAIN_PICTURE_ID = 1;
    private const HOVER_PICTURE_ID = 4;
    private const FALLBACK_HOVER_PICTURE_ID = 2;

    /** @var string */
    private $hoverPicturePath;
    /** @var string */
    private $mainPicturePath;
    /** @var ArchiveFileInterface */
    private $archiveFiles;
    /** @var string */
    private $fullPictureName;

    /**
     * @throws InterpreterException
     */
    public function resolveTargetDirectory(string $identityValue): string
    {
        if (((int) $identityValue) === 0) {
            throw new InterpreterException('Could not resolve identity value to positive integer');
        }

        return ($identityValue % 1000).DIRECTORY_SEPARATOR.$identityValue.DIRECTORY_SEPARATOR;
    }

    /**
     * @throws InterpreterException
     */
    public function arrangeArchiveFiles(ArchiveFilesInterface $archiveFiles): array
    {
        $this->archiveFiles = $archiveFiles;
        $mainAndHoverPaths = [];
        $otherPaths = [];

        /** @var ArchiveFileInterface $file */
        foreach ($this->archiveFiles as $file) {

            if ($file->getSaveFullPath() === $this->getMainPictureSavePath()) {
                array_unshift($mainAndHoverPaths, $file->getSaveFullPath());
            } elseif ($file->getSaveFullPath() === $this->getHoverPictureSavePath()) {
                array_push($mainAndHoverPaths, $file->getSaveFullPath());
            } else {
                array_push($otherPaths, $file->getSaveFullPath());
            }
        }

        return array_merge($mainAndHoverPaths, $otherPaths);
    }

    /**
     * @throws InterpreterException
     */
    private function getHoverPictureSavePath(): string
    {
        if ($this->hoverPicturePath === null) {
            $this->handlePicturesPaths();
        }

        return $this->hoverPicturePath;
    }

    /**
     * @throws InterpreterException
     */
    private function getMainPictureSavePath(): string
    {
        if ($this->mainPicturePath === null) {
            $this->handlePicturesPaths();
        }

        return $this->mainPicturePath;
    }

    /**
     * @throws InterpreterException
     */
    private function handlePicturesPaths(): void
    {
        /** @var ArchiveFileInterface $archiveFile */
        foreach ($this->archiveFiles as $archiveFile) {
            $this->fullPictureName = $archiveFile->getBasename();//todo czy to jest potrzebne $this->fullPictureName ????
            $this->handleFullPictureName($archiveFile);
            if ($this->picturePosition === self::MAIN_PICTURE_ID) {
                $this->mainPicturePath = $archiveFile->getSaveFullPath();
            } elseif ($this->picturePosition === self::FALLBACK_HOVER_PICTURE_ID) {
                $this->hoverPicturePath = $archiveFile->getSaveFullPath();
            } elseif ($this->picturePosition === self::HOVER_PICTURE_ID) {
                $this->hoverPicturePath = $archiveFile->getSaveFullPath();
            }
        }

        if (empty($this->mainPicturePath)) {
            throw new InterpreterException('Could not resolve main picture path from set of pictures');
        }
    }
}
