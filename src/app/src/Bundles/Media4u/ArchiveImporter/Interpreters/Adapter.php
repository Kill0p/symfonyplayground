<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

class Adapter implements AdapterInterface
{
    /** @var ProductNameInterface */
    private $productNameInterpreter;
    /** @var ProductNameSetInterface */
    private $productNameSetInterpreter;

    public function __construct(
        ProductNameInterface $productNameInterpreter,
        ProductNameSetInterface $productNameSetInterpreter
    ) {
        $this->productNameInterpreter = $productNameInterpreter;
        $this->productNameSetInterpreter = $productNameSetInterpreter;
    }

    public function getSaveFullPath(ArchiveFileInterface $archiveFile): string
    {
        return $this->productNameInterpreter->getFormattedPictureBaseName($archiveFile);
    }

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function resolveIdentityKeysForArchiveFiles(ArchiveFilesInterface $archiveFiles): ArchiveFilesInterface
    {
        return $this->productNameInterpreter->resolveIdentityKeys($archiveFiles);
    }

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function getTargetDirectoryNameForArchiveFile(string $identityValue): string
    {
        return $this->productNameSetInterpreter->resolveTargetDirectory($identityValue);
    }

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function arrangeArchiveFiles(ArchiveFilesInterface $archiveFiles): array
    {
        return $this->productNameSetInterpreter->arrangeArchiveFiles($archiveFiles);
    }
}
