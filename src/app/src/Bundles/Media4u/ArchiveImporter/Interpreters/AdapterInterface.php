<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

interface AdapterInterface
{
    public function getSaveFullPath(ArchiveFileInterface $archiveFile): string;

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function resolveIdentityKeysForArchiveFiles(ArchiveFilesInterface $archiveFiles): ArchiveFilesInterface;

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function getTargetDirectoryNameForArchiveFile(string $identityValue): string;

    /**
     * @throws \Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException
     */
    public function arrangeArchiveFiles(ArchiveFilesInterface $archiveFiles): array;
}
