<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Resolvers\IdentityKeyResolverInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

class ProductName implements ProductNameInterface
{
    use ProductNameTrait;

    private static $NUMBER_OF_PIECES_IN_PICTURE_NAME = 2;
    private static $DELIMITER = ';;';

    /** @var IdentityKeyResolverInterface */
    private $identityKeyResolver;

    public function __construct(
        IdentityKeyResolverInterface $identityKeyResolver
    ) {
        $this->identityKeyResolver = $identityKeyResolver;
    }

    /**
     * @throws InterpreterException
     */
    public function getFormattedPictureBaseName(ArchiveFileInterface $archiveFile): string
    {
        $this->handleFullPictureName($archiveFile);
        $archiveFile->setFormattedBaseName($this->pictureName.'_'.$this->picturePosition.'.'.$this->pictureExtension);

        return $archiveFile->getFormattedBaseName();
    }

    /**
     * @throws InterpreterException
     */
    public function resolveIdentityKeys(ArchiveFilesInterface $archiveFiles): ArchiveFilesInterface
    {
        try {
            $this->identityKeyResolver->acquireIdentityValuesForFiles($archiveFiles);
        } catch (\Exception $e) {
            throw new InterpreterException('Could not successfully acquire identity values for files', 0, $e);
        }

        return $archiveFiles->validateIdentityKeyValueSet();
    }
}
