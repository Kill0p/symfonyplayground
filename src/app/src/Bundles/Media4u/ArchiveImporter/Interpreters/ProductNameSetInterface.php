<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

interface ProductNameSetInterface
{
    /**
     * @throws InterpreterException
     */
    public function resolveTargetDirectory(string $identityValue): string;

    /**
     * @throws InterpreterException
     */
    public function arrangeArchiveFiles(ArchiveFilesInterface $archiveFiles): array;
}
