<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;

interface ProductNameInterface
{
    public function getFormattedPictureBaseName(ArchiveFileInterface $archiveFile): string;

    /**
     * @throws InterpreterException
     */
    public function resolveIdentityKeys(ArchiveFilesInterface $archiveFiles): ArchiveFilesInterface;
}
