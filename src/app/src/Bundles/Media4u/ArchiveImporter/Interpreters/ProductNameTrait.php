<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Interpreters;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;

trait ProductNameTrait
{
    private static $NUMBER_OF_PIECES_IN_PICTURE_NAME = 2;
    private static $DELIMITER = ';;';

    /** @var string */
    protected $pictureName;
    /** @var int */
    protected $picturePosition;
    /** @var string */
    protected $pictureExtension;

    /**
     * @throws InterpreterException
     */
    private function handleFullPictureName(\Media4u\ArchiveImporter\Importer\ArchiveFileInterface $archiveFile): void
    {
        $nameElements = explode(static::$DELIMITER, $archiveFile->getBasename());

        if (count($nameElements) !== static::$NUMBER_OF_PIECES_IN_PICTURE_NAME) {
            throw new InterpreterException(
                'There should be exactly ' . static::$NUMBER_OF_PIECES_IN_PICTURE_NAME . ' pieces which picture name consist of'
            );
        }

        $this->pictureName = $nameElements[0];
        $this->handlePicturePositionAndExtension($nameElements[1]);
    }

    /**
     * @throws InterpreterException
     */
    private function handlePicturePositionAndExtension(string $pictureNameLastElement): void
    {
        $picturePathInfo = pathinfo($pictureNameLastElement);
        $this->picturePosition = (int) $picturePathInfo['filename'];

        if (!array_key_exists('extension', $picturePathInfo)) {
            throw new InterpreterException('Could not get extension for file');
        }
        $this->pictureExtension = $picturePathInfo['extension'];
    }
}
