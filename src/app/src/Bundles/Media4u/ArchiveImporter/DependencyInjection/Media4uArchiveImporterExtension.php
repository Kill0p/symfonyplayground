<?php
declare(strict_types=1);
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 16.01.2020
 */
namespace Media4u\ArchiveImporter\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class Media4uArchiveImporterExtension extends Extension
{
    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('segregators.xml');

        $container->setParameter('media4u_archive_importer.identity_key_resolver.class', $config['identity_key_resolver']['class']);
    }
}
