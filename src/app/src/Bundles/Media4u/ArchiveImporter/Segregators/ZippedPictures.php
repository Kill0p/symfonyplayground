<?php
declare(strict_types=1);
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 16.01.2020
 */
namespace Media4u\ArchiveImporter\Segregators;

use Media4u\ArchiveImporter\Exception\Segregators\SegregatorException;
use Media4u\ArchiveImporter\Resolvers\FileResolverInterface;

class ZippedPictures implements SegregatorInterface
{
    /** @var ZippedPicturesPathsInterface */
    private $paths;
    /** @var FileResolverInterface */
    private $fileResolver;
    /** @var string */
    private $archiveDateDelimiter;
    /** @var int */
    private $timestampForSegregationProcess;
    /** @var string */
    private $timezone;
    /** @var int */
    private $minimumMonthsFoldersLeft;

    public function __construct(
        ZippedPicturesPathsInterface $paths,
        FileResolverInterface $fileResolver,
        int $minimumMonthsFoldersLeft = 3,
        string $archiveDateDelimiter = '.',
        string $timezone = 'local'
    ) {
        $this->paths = $paths;
        $this->fileResolver = $fileResolver;
        $this->minimumMonthsFoldersLeft = $minimumMonthsFoldersLeft;
        $this->archiveDateDelimiter = $archiveDateDelimiter;
        $this->timezone = $timezone;
    }

    /**
     * @throws SegregatorException
     * @throws \Exception
     */
    public function handleArchive(): void
    {
        $this->moveArchive();
        $this->cleanUpArchiveDirectory();
    }

    /**
     * @throws SegregatorException
     */
    private function moveArchive(): void
    {
        $newArchiveFileFullPath = $this->getNewArchiveFileFullPath();

        try {
            $this->fileResolver->moveArchive($this->paths->getArchiveFileUploadFullPath(), $this->getNewArchiveFileName(), $newArchiveFileFullPath);
        } catch (\Exception $e) {
            throw new SegregatorException('Error where moving archive from '.$this->paths->getArchiveFileUploadFullPath().' to '.$newArchiveFileFullPath, 0, $e);
        }
    }

    /**
     * @throws SegregatorException
     */
    private function getNewArchiveFileFullPath(): string
    {
        $currentTimeStamp = $this->getTimestampForSegregationStamp();
        $newArchiveFileName = $this->getNewArchiveFileName();
        $yearAndMonthDate = date('Y', $currentTimeStamp).$this->archiveDateDelimiter.date('m', $currentTimeStamp);

        return
            $this->paths->getProjectRootDirectoryName()
            .DIRECTORY_SEPARATOR
            .$this->paths->getArchiveTargetRelativePath()
            .$yearAndMonthDate
            .DIRECTORY_SEPARATOR
            .$newArchiveFileName;
    }

    /**
     * @throws SegregatorException
     */
    private function getNewArchiveFileName(): string
    {
        $currentTimeStamp = $this->getTimestampForSegregationStamp();
        $fileName = $this->prepareFileBaseOnTimestamp($currentTimeStamp);

        $fileExtension = pathinfo($this->paths->getArchiveFileUploadFullPath(), PATHINFO_EXTENSION);

        return $fileName.'.'.$fileExtension;
    }

    /**
     * @throws SegregatorException
     */
    private function getTimestampForSegregationStamp(): int
    {
        if ($this->timestampForSegregationProcess === null) {
            $this->timestampForSegregationProcess = $this->getCurrentTimeStamp();
        }

        return $this->timestampForSegregationProcess;
    }

    /**
     * @throws SegregatorException
     */
    private function getCurrentTimeStamp(): int
    {
        if ($this->timezone === 'local' ||  $this->timezone === date_default_timezone_get()) {

            return time();
        }

        try {
            $datetime = new \DateTime('now', new \DateTimeZone($this->timezone));
        } catch (\Exception $e) {
            throw new SegregatorException('Could not create current time stamp', 0, $e);
        }

        sscanf($datetime->format('j-n-Y G:i:s'), '%d-%d-%d %d:%d:%d', $day, $month, $year, $hour, $minute, $second);

        return mktime($hour, $minute, $second, $month, $day, $year);
    }

    /**
     * @throws SegregatorException
     */
    private function cleanUpArchiveDirectory(): void
    {
        $archiveTargetDirectoryName = $this->paths->getProjectRootDirectoryName().DIRECTORY_SEPARATOR.$this->paths->getArchiveTargetRelativePath();
        try {
            $archiveDirectoryContent = $this->fileResolver->listContent($archiveTargetDirectoryName);
        } catch (\Exception $e) {
            throw new SegregatorException('Error when list content of archive directory '.$this->paths->getArchiveTargetRelativePath(), 0, $e);
        }

        if ($this->isAcceptableThresholdOfFolders(count($archiveDirectoryContent))) {

            return;
        }

        $archiveDirectoryContent->naturalSortByPath();
        $fileNamesToFilterOutAmount = count($archiveDirectoryContent) - $this->minimumMonthsFoldersLeft;
        $filesToRemove = $archiveDirectoryContent->slice($fileNamesToFilterOutAmount);

        foreach ($filesToRemove as $file) {
            try {
                if ($file->getType() === FileInterface::TYPE_DIRECTORY) {
                    $this->fileResolver->deleteDirectory($file->getFullPath());
                } else {
                    $this->fileResolver->deleteFile($file->getFullPath());
                }
            } catch (\Exception $e) {
                throw new SegregatorException("Could not clean up archive content {$file->getFullPath()}", 0, $e);
            }
        }
    }

    /**
     * @throws SegregatorException
     */
    public function getUnpackDestinationDirectoryName(): string
    {
        $currentTimestamp = $this->getTimestampForSegregationStamp();
        $destination = $this->prepareFileBaseOnTimestamp($currentTimestamp);

        return $this->paths->getProjectRootDirectoryName()
            .DIRECTORY_SEPARATOR
            .$this->paths->getPicturesTargetRelativePath()
            .$destination
            .DIRECTORY_SEPARATOR;
    }

    private function prepareFileBaseOnTimestamp(int $currentTimestamp): string
    {
        return date('Y', $currentTimestamp)
            .$this->archiveDateDelimiter.date('m', $currentTimestamp)
            .$this->archiveDateDelimiter.date('d', $currentTimestamp)
            .$this->archiveDateDelimiter.date('G', $currentTimestamp)
            .$this->archiveDateDelimiter.date('i', $currentTimestamp)
            .$this->archiveDateDelimiter.date('s', $currentTimestamp);
    }

    private function isAcceptableThresholdOfFolders(int $currentNumberOfFolders): bool
    {
        return $currentNumberOfFolders <= $this->minimumMonthsFoldersLeft;
    }
}
