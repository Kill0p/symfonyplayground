<?php
declare(strict_types=1);
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 16.01.2020
 */
namespace Media4u\ArchiveImporter\Segregators;

use Media4u\ArchiveImporter\Exception\FileResolvers\NoContentException;
use Media4u\ArchiveImporter\Exception\Segregators\SegregatorException;

interface SegregatorInterface
{
    /**
     * @throws SegregatorException
     * @throws NoContentException
     * @throws \Exception
     */
    public function handleArchive(): void;

    /**
     * @throws SegregatorException
     */
    public function getUnpackDestinationDirectoryName(): string;
}
