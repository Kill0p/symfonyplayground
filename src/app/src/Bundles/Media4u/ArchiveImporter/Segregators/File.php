<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Segregators;

class File implements FileInterface
{
    /** @var int */
    private $type;
    /** @var string */
    private $fullPath;

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getFullPath(): string
    {
        return $this->fullPath;
    }

    public function setFullPath(string $fullPath): void
    {
        $this->fullPath = $fullPath;
    }
}
