<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Segregators;

class ZippedPicturesPaths implements ZippedPicturesPathsInterface
{
    /** @var string  */
    private $archiveFileUploadFullPath;
    /** @var string  */
    private $picturesImportRelativePath;
    /** @var string  */
    private $archiveTargetRelativePath;
    /** @var string  */
    private $picturesTargetRelativePath;
    /** @var string */
    private $projectRootDirectoryName;

    public function __construct(
        string $picturesImportRelativePath,
        string $archiveTargetRelativePath,
        string $picturesTargetRelativePath,
        string $projectRootDirectoryName,
        string $archiveFileUploadFullPath = ''
    ) {
        $this->archiveFileUploadFullPath = $archiveFileUploadFullPath;
        $this->picturesImportRelativePath = $picturesImportRelativePath;
        $this->archiveTargetRelativePath = $archiveTargetRelativePath;
        $this->picturesTargetRelativePath = $picturesTargetRelativePath;
        $this->projectRootDirectoryName = $projectRootDirectoryName;
    }

    public function getArchiveFileUploadFullPath(): string
    {
        return $this->archiveFileUploadFullPath;
    }

    public function setArchiveFileUploadFullPath(string $archiveFileUploadFullPath): void
    {
        $this->archiveFileUploadFullPath = $archiveFileUploadFullPath;
    }

    public function getPicturesImportRelativePath(): string
    {
        return $this->picturesImportRelativePath;
    }

    public function setPicturesImportRelativePath(string $picturesImportRelativePath): void
    {
        $this->picturesImportRelativePath = $picturesImportRelativePath;
    }

    public function getArchiveTargetRelativePath(): string
    {
        return $this->archiveTargetRelativePath;
    }

    public function setArchiveTargetRelativePath(string $archiveTargetRelativePath): void
    {
        $this->archiveTargetRelativePath = $archiveTargetRelativePath;
    }

    public function getPicturesTargetRelativePath(): string
    {
        return $this->picturesTargetRelativePath;
    }

    public function setPicturesTargetRelativePath(string $picturesTargetRelativePath): void
    {
        $this->picturesTargetRelativePath = $picturesTargetRelativePath;
    }

    public function getProjectRootDirectoryName(): string
    {
        return $this->projectRootDirectoryName;
    }

    public function setProjectRootDirectoryName(string $projectRootDirectoryName): void
    {
        $this->projectRootDirectoryName = $projectRootDirectoryName;
    }
}
