<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Segregators;

interface FileInterface
{
    public const TYPE_DIRECTORY = 0;
    public const TYPE_FILE = 1;

    public function setType(int $type): void;

    public function setFullPath(string $fullPath): void;

    public function getType(): int;

    public function getFullPath(): string;

}
