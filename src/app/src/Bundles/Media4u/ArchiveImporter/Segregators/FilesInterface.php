<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Segregators;

interface FilesInterface extends \ArrayAccess, \Countable
{
    public function offsetGet($index): FileInterface;

    public function current(): FileInterface;

    public function naturalSortByPath(): void;

    public function slice(int $length): Files;
}
