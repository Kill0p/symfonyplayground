<?php
declare(strict_types=1);

namespace Media4u\ArchiveImporter\Segregators;

class Files extends \ArrayIterator implements FilesInterface
{
    public function __construct(File ...$files)
    {
        parent::__construct($files);
    }

    public function offsetGet($index): FileInterface
    {
        return parent::offsetGet($index);
    }

    public function current(): FileInterface
    {
        return parent::current();
    }

    public function naturalSortByPath(): void
    {
        $this->uasort(function(File $a, File $b) {
            return strnatcmp($a->getFullPath(), $b->getFullPath());
        });
    }

    public function slice(int $length): Files
    {
        $sliced = array_slice(iterator_to_array($this, true), 0, $length, true);

        return new static(...$sliced);
    }
}
