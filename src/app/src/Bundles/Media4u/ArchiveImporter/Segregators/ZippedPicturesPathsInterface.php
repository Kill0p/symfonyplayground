<?php
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 16.01.2020
 */

namespace Media4u\ArchiveImporter\Segregators;

interface ZippedPicturesPathsInterface
{
    public function getArchiveFileUploadFullPath(): string;

    public function setArchiveFileUploadFullPath(string $archiveFileUploadPath): void;

    public function getPicturesImportRelativePath(): string;

    public function setPicturesImportRelativePath(string $picturesImportDirectoryName): void;

    public function getArchiveTargetRelativePath(): string;

    public function setArchiveTargetRelativePath(string $archiveTargetDirectoryName): void;

    public function getPicturesTargetRelativePath(): string;

    public function setPicturesTargetRelativePath(string $picturesTargetDirectoryName): void;

    public function setProjectRootDirectoryName(string $projectRootDirectoryName): void;

    public function getProjectRootDirectoryName(): string;
}