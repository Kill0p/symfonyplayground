<?php
declare(strict_types=1);

namespace Media4u\Framework\ActiveRecord\Serialization;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ResourceSerializer implements ResourceSerializerInterface
{
    /** @var XmlEncoder */
    private $xmlEncoder;
    /** @var JsonEncoder */
    private $jsonEncode;

    public function __construct(
        XmlEncoder $xmlEncoder,
        JsonEncoder $jsonEncode
    ) {
        $this->xmlEncoder = $xmlEncoder;
        $this->jsonEncode = $jsonEncode;
    }

    public function serialize($data, int $method): string
    {
        switch ($method) {
            case ResourceSerializerInterface::JSON:
                $value = $this->jsonEncode->encode($data, JsonEncoder::FORMAT);break;
            case ResourceSerializerInterface::XML:
                $value = $this->xmlEncoder->encode($data,XmlEncoder::FORMAT);break;
            default:
                throw new \InvalidArgumentException('Specified method of serialization is not configured');
        }

        return $value;
    }

    public function deserialize($data, int $method)
    {
        switch ($method) {
            case ResourceSerializerInterface::JSON:
                $value = $this->jsonEncode->decode($data, JsonEncoder::FORMAT);break;
            case ResourceSerializerInterface::XML:
                $value = $this->xmlEncoder->decode($data,XmlEncoder::FORMAT);break;
            default:
                throw new \InvalidArgumentException('Specified method of deserialization is not configured');
        }

        return $value;
    }
}
