<?php
declare(strict_types=1);

namespace Media4u\Framework\ActiveRecord\Serialization;

class SerializableFieldConfig
{
    /** @var int */
    private $serializationMethod;
    /** @var int*/
    private $deserializationMethod;
    /** @var bool */
    private $unsetEmptyWhenSerializing;
    /** @var mixed */
    private $defaultValueForSerialization;
    /** @var mixed */
    private $defaultValueForDeserialization;

    public function __construct(
        int $serializationMethod,
        int $deserializationMethod,
        bool $unsetEmptyWhenSerializing,
        $defaultValueForSerialization,
        $defaultValueForDeserialization
    ) {
        $this->serializationMethod = $serializationMethod;
        $this->deserializationMethod = $deserializationMethod;
        $this->unsetEmptyWhenSerializing = $unsetEmptyWhenSerializing;
        $this->defaultValueForSerialization = $defaultValueForSerialization;
        $this->defaultValueForDeserialization = $defaultValueForDeserialization;
    }

    public function getSerializationMethod(): int
    {
        return $this->serializationMethod;
    }

    public function setSerializationMethod(int $serializationMethod): void
    {
        $this->serializationMethod = $serializationMethod;
    }

    public function getDeserializationMethod(): int
    {
        return $this->deserializationMethod;
    }

    public function setDeserializationMethod(int $deserializationMethod): void
    {
        $this->deserializationMethod = $deserializationMethod;
    }

    public function isUnsetEmptyWhenSerializing(): bool
    {
        return $this->unsetEmptyWhenSerializing;
    }

    public function setUnsetEmptyWhenSerializing(bool $unsetEmptyWhenSerializing): void
    {
        $this->unsetEmptyWhenSerializing = $unsetEmptyWhenSerializing;
    }

    public function getDefaultValueForSerialization()
    {
        return $this->defaultValueForSerialization;
    }

    public function setDefaultValueForSerialization($defaultValueForSerialization): void
    {
        $this->defaultValueForSerialization = $defaultValueForSerialization;
    }

    public function getDefaultValueForDeserialization()
    {
        return $this->defaultValueForDeserialization;
    }

    public function setDefaultValueForDeserialization($defaultValueForDeserialization): void
    {
        $this->defaultValueForDeserialization = $defaultValueForDeserialization;
    }
}
