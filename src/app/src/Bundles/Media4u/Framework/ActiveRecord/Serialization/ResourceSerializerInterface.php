<?php
declare(strict_types=1);

namespace Media4u\Framework\ActiveRecord\Serialization;

interface ResourceSerializerInterface
{
    public const JSON = 1;
    public const XML = 2;

    public function serialize($data, int $method): string;
    public function deserialize($data, int $method);
}
