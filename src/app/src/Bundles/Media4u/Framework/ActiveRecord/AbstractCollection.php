<?php
declare(strict_types=1);

namespace Media4u\Framework\ActiveRecord;

use Doctrine\DBAL\Connection;
use Media4u\Framework\Common\CollectionObject;
use Doctrine\Common\Persistence\ManagerRegistry;
use Media4u\Framework\Common\DataObject;
use Symfony\Contracts\Cache\ItemInterface;

// https://symfony.com/doc/4.4/cache.html#configuring-cache-with-frameworkbundle
// uzycie https://www.doctrine-project.org/projects/doctrine-dbal/en/2.10/reference/caching.html
// todo logera ma dbal, chyba ze chcemy miec cos swojego jeszcze dodatkowo
abstract class AbstractCollection extends CollectionObject
{
    protected const CACHE_EXPIRE_AT_DEFAULT = 60;
    /** @var string */
    private $entityClass;
    /** @var AbstractResource */
    private $resource;
    /** @var ManagerRegistry */
    private $managerRegistry;
    /** @var Connection */
    private $connection;
    /** @var \Doctrine\DBAL\Query\QueryBuilder */
    private $queryBuilder;
    /**
     * Do we want to set into entity object 'data' and also 'original data' (which is raw fetched data which should not be
     * manipulated during living time of entity object)
     * @var bool
     */
    protected $isSettingOriginalData = true;
    /** @var \Symfony\Contracts\Cache\CacheInterface */
    private $cache;
    /** @var string */
    private $cacheKey;
    /** @var int */
    private $cacheInterval = self::CACHE_EXPIRE_AT_DEFAULT;

    public function __construct(
        string $entityClass,
        AbstractResource $resource,
        ManagerRegistry $managerRegistry,
        \Symfony\Contracts\Cache\CacheInterface $cache,
        string $connectionName = ''
    ) {
        $this->entityClass = $entityClass;
        $this->resource = $resource;
        $this->managerRegistry = $managerRegistry;
        $this->cache = $cache;
        $this->createQueryBuilder($connectionName);
        $this->initSelectBuilder();
    }

    protected function createQueryBuilder(string $connectionName = ''): void
    {
        if (empty($connectionName)) {
            $this->queryBuilder = $this->getConnection()->createQueryBuilder();
        } else {
            $this->connection = $this->managerRegistry->getConnection($connectionName);
            $this->queryBuilder = $this->connection->createQueryBuilder();
        }
    }

    protected function getConnectionName(): ?string
    {
        return null;
    }

    protected function getConnection(): \Doctrine\DBAL\Connection
    {
        if ($this->connection === null) {
            $this->connection = $this->managerRegistry->getConnection($this->getConnectionName());
        }

        return $this->connection;
    }

    /**
     * @throws \Media4u\Framework\Exception\FrameworkException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function load(): void
    {
        if ($this->isLoaded()) {

            return;
        }

        $this->loadData();
    }

    /**
     * @throws \Media4u\Framework\Exception\FrameworkException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function loadData(): void
    {
        $this->beforeLoadData();
        if ($this->getCacheKey() !== null) {
            $data = $this->doCache(
                $this->getCacheKey(),
                function() {
                //todo mozna dodac tryb miedzy fetchaAll a fetchColumn
                return $this->getQueryBuilder()->execute()->fetchAll();
                },
                function(ItemInterface $item) {
                    $this->doCacheConfigurationOnLoadDataFor($item);
                }
            );
        } else {
            $data = $this->getQueryBuilder()->execute()->fetchAll();
        }

        if (is_array($data)) {
            /**
             * @var int $key
             * @var array $item
             */
            foreach ($data as $key => $item) {
                $entity = $this->getNewEmptyItem();
                $entity->setData($item);
                $this->resource->deserializeFields($entity);
                if ($this->isSettingOriginalData) {
                    $entity->setOriginalData();
                }
                $this->addItem($entity);
            }
        }

        $this->setIsLoaded();
        $this->clearCacheKey();
        $this->resetCacheInterval();
        $this->afterLoadData();
    }

    /**
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function doCache(string $key, callable $heavyComputation, callable $cacheInterval)
    {
        return $this->cache->get($key, function (ItemInterface $item) use ($heavyComputation, $cacheInterval) {
            $result = $heavyComputation();
            $cacheInterval($item);

            return $result;
        });
    }

    protected function doCacheConfigurationOnLoadDataFor(ItemInterface $item): void
    {
        $item->expiresAfter($this->getCacheInterval());
    }

    public function setCacheInterval(int $expireAfter): void
    {
        $this->cacheInterval = $expireAfter;
    }

    public function getCacheInterval(): int
    {
        return $this->cacheInterval;
    }

    public function getCacheKey(): ?string
    {
        return $this->cacheKey;
    }

    public function setCacheKey(string $cacheKey): void
    {
        $this->cacheKey = $cacheKey;
    }

    public function getSql(): string
    {
        return $this->getQueryBuilder()->getSQL();
    }

    protected function getItemId(DataObject $dataObject)
    {
        return $dataObject->getData($this->getResource()->getPrimaryKey());
    }

    public function getResource(): AbstractResource
    {
        return $this->resource;
    }

    /**
     * @return AbstractEntity[]
     */
    public function getItems(): array
    {
        return parent::getItems();
    }

    /**
     * Cached
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getSize(): int
    {
        if ($this->getCacheKey() !== null) {
            $this->doCache(
                $this->getCacheKeyForSize(),
                function() {
                    $sql = $this->getCountSql();
                    $this->itemsAmount = (int) $this->getConnection()->fetchColumn($sql);
                },
                function(ItemInterface $item) {
                    $this->doCacheConfigurationOnGetSizeFor($item);
                }
            );
        } else {
            if ($this->itemsAmount === null) {
                $sql = $this->getCountSql();
                $this->itemsAmount = (int) $this->getConnection()->fetchColumn($sql);
            }
        }

        return $this->itemsAmount;
    }

    protected function getCountSql(): string
    {
        $queryBuilder = clone $this->getQueryBuilder();
        $queryBuilder->resetQueryParts(['select', 'distinct', 'orderBy']);

        if (!count($this->getQueryBuilder()->getQueryPart('groupBy'))) {
            $queryBuilder->add('select', 'COUNT(*)');

            return $queryBuilder->getSQL();
        }

        $queryBuilder->resetQueryPart('groupBy');
        $group = $this->getQueryBuilder()->getQueryPart('groupBy');
        $queryBuilder->add('select', 'COUNT(*)');
        $queryBuilder->from("(SELECT DISTINCT ".implode(", ", $group)." FROM ".$this->getTableName().")", 'temporary');

        return $queryBuilder->getSQL();

    }

    protected function getCacheKeyForSize(): string
    {
        return $this->getCacheKey().'_size';
    }

    protected function doCacheConfigurationOnGetSizeFor(ItemInterface $item): void
    {
        $item->expiresAfter($this->getCacheInterval());
    }

    public function getQueryBuilder(): \Doctrine\DBAL\Query\QueryBuilder
    {
        return $this->queryBuilder;
    }

    /**
     * Template method
     * Initialize 'must have' query part for collection
     */
    protected function initSelectBuilder(): void
    {
        $this->getQueryBuilder()
            ->select('*')
            ->from($this->getTableName(), 'main_table');
    }

    protected function getNewEmptyItem(): AbstractEntity
    {
        return new $this->entityClass;
    }

    public function setEntityClass(string $entityClass): void
    {
        $this->entityClass = $entityClass;
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    protected function afterLoadData(): void
    {

    }

    protected function beforeLoadData(): void
    {

    }

    protected function getTableName(): string
    {
        return $this->getResource()->getTableName();
    }

    private function clearCacheKey(): void
    {
        $this->cacheKey = null;
    }

    private function resetCacheInterval(): void
    {
        $this->cacheInterval = self::CACHE_EXPIRE_AT_DEFAULT;
    }
}
