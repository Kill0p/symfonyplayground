<?php
declare(strict_types=1);

namespace Media4u\Framework\ActiveRecord;

use Media4u\Framework\Common\DataObject;

abstract class AbstractEntity extends DataObject
{
    /** @var bool */
    private $isDeleted = false;

    private $originalData = [];

    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    public function setOriginalData($key = null, $data = null): void
    {
        if ($key === null) {
            $this->originalData = $this->data;
        } else {
            $this->originalData[$key] = $data;
        }
    }

    public function getOriginalData($key = null)
    {
        if ($key === null) {

            return $this->originalData;
        }

        if (isset($this->originalData[$key])) {

            return $this->originalData[$key];
        }

        return null;
    }

    public function hasDataChangedFor(string $key): bool
    {
        return $this->getData($key) != $this->getOriginalData($key);
    }
}
