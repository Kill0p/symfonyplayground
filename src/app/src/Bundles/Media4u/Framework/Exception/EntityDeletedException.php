<?php
declare(strict_types=1);

namespace Media4u\Framework\Exception;

class EntityDeletedException extends DatabaseException
{

}
