<?php
declare(strict_types=1);

namespace Media4u\Framework\Common;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class DataObjectSimpleSerializer
{
    /** @var SerializerInterface */
    private $serializer;
    /** @var \Symfony\Component\Serializer\Encoder\EncoderInterface */
    private $xmlEncoder;
    /** @var */
    private $jsonEncode;
    /** @var ObjectNormalizer */
    private $objectNormalizer;

    public function __construct(
        XmlEncoder $xmlEncoder,
        JsonEncoder $jsonEncode,
        ObjectNormalizer $objectNormalizer
    ) {
        $this->xmlEncoder = $xmlEncoder;
        $this->jsonEncode = $jsonEncode;
        $this->objectNormalizer = $objectNormalizer;
    }


    public function toXml(DataObject $dataObject): string
    {
        $serializer = $this->getSerializer();

        return $serializer->serialize($dataObject->getData(), 'xml');
    }

    public function toJson(DataObject $dataObject): string
    {
        $serializer = $this->getSerializer();

        return $serializer->serialize($dataObject->getData(), 'json');
    }

    /**
     * Lazy initialization
     */
    public function getSerializer(): SerializerInterface
    {
        if (empty($serializer)) {

            $this->serializer = new Serializer([], [$this->xmlEncoder, $this->jsonEncode]);
        }

        return $this->serializer;
    }
}
