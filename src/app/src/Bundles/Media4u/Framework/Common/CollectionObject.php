<?php
declare(strict_types=1);

namespace Media4u\Framework\Common;

use Media4u\Framework\Exception\FrameworkException;

class CollectionObject implements \IteratorAggregate, \Countable
{
    /** @var array */
    protected $items;
    /** @var int */
    protected $itemsAmount;
    /** @var bool */
    protected $isLoaded = false;
    /** @var int|null */
    private $pageSize = null;
    /** @var int */
    private $currentPage;
    /** @var array */
    protected $flags;

    /**
     * @inheritDoc
     */
    public function getIterator(): iterable
    {
        $this->load();

        return new \ArrayIterator($this->items);
    }

    public function load(): void
    {
        $this->loadData();
    }

    protected function loadData(): void
    {

    }

    public function count(): int
    {
        $this->load();
        return count($this->items);
    }

    /**
     * Cached
     */
    public function getSize(): int
    {
        $this->load();
        if ($this->itemsAmount === null) {
            $this->itemsAmount = count($this->getItems());
        }

        return $this->itemsAmount;
    }

    /**
     * @throws FrameworkException
     */
    public function addItem(DataObject $dataObject): void
    {
        $itemId = $this->getItemId($dataObject);

        if (!empty($itemId) ) {
            if (isset($this->items[$itemId])) {
                throw new FrameworkException('Item '.get_class($dataObject).' with the same ID '.$this->getItemId($dataObject).' already exists.');
            }
            $this->items[$itemId] = $dataObject;
        } else {
            $this->items[] = $dataObject;
        }
    }

    public function getItemById($id): ?DataObject
    {
        $this->load();

        if (isset($this->items[$id])) {

            return $this->items[$id];
        }

        return null;
    }

    public function toArray($fields = []): array
    {
        $items = [];
        $items['size'] = $this->getSize();
        $items['items'] = [];

        /** @var DataObject $item */
        foreach ($this as $item) {
            $items['items'][] = $item->toArray($fields);
        }

        return $items;
    }

    protected function getItemId(DataObject $dataObject)
    {
        return $dataObject->getData('id');
    }

    public function getItems(): array
    {
        $this->load();

        return $this->items;
    }

    protected function setIsLoaded(bool $isLoaded = true): void
    {
        $this->isLoaded = $isLoaded;
    }

    public function isLoaded(): bool
    {
        return $this->isLoaded;
    }

    public function clear(): void
    {
        $this->items = [];
        $this->itemsAmount = null;
        $this->setIsLoaded(false);
    }

    public function removeAllItems(): void
    {
        $this->items = [];
        $this->itemsAmount = null;
    }

    public function removeItemByKey($key): void
    {
        if (isset($this->items[$key])) {
            unset($this->items[$key]);
        }
    }

    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    public function getCurrentPage(int $displacement = 0): int
    {
        if ($this->currentPage + $displacement < 1) {

            return 1;
        } elseif ($this->currentPage + $displacement > $this->getLastPageNumber()) {

            return $this->getLastPageNumber();
        } else {

            return $this->currentPage + $displacement;
        }
    }


    public function getLastPageNumber(): int
    {
        $collectionSize = $this->getSize();

        if (0 === $collectionSize) {

            return 1;
        } elseif ($this->getPageSize()) {
            $ceilResult = ceil($collectionSize / $this->getPageSize());

            if ($ceilResult === false) {
                throw new \RuntimeException('Could not round up number');
            }

            return (int) $ceilResult;
        } else {

            return 1;
        }
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function setPageSize(int $size): void
    {
        $this->pageSize = $size;
    }

    public function getFlag(string $flag)
    {
        return $this->flags[$flag] ?? null;
    }

    public function setFlag(string $flag, $value = null): void
    {
        $this->flags[$flag] = $value;
    }

    public function hasFlag(string $flag): bool
    {
        return array_key_exists($flag, $this->flags);
    }
}
