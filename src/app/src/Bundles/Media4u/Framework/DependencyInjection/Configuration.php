<?php
declare(strict_types=1);

namespace Media4u\Framework\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('media4u_framework');

//        $treeBuilder->getRootNode()
//            ->children()
//                ->arrayNode('identity_key_resolver')
//                    ->children()
//                        ->scalarNode('class')->isRequired()
//                        ->end()
//                    ->end()
//                ->end()
//            ->end();

        return $treeBuilder;
    }
}
