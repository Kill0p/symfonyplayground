<?php
declare(strict_types=1);

namespace Media4u\Framework\Event\ActiveRecord;

use Symfony\Contracts\EventDispatcher\Event;

class BeforeLoad extends Event
{
    /** @var array */
    private $columns;
    /** @var array */
    private $values;

    public function __construct(array $columns, array $values)
    {
        $this->columns = $columns;
        $this->values = $values;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getValues(): array
    {
        return $this->values;
    }
}
