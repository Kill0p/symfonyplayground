<?php
declare(strict_types=1);

namespace Media4u\Framework\Event\ActiveRecord;

use Media4u\Framework\ActiveRecord\AbstractEntity;
use Symfony\Contracts\EventDispatcher\Event;

class AfterLoad extends Event
{
    /** @var AbstractEntity */
    private $entity;

    public function __construct(
        AbstractEntity $entity
    ) {
        $this->entity = $entity;
    }

    public function getEntity(): AbstractEntity
    {
        return $this->entity;
    }
}
