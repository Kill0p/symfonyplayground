<?php
namespace App\DocumentFactory;

use App\Document\Log;

class LogFactory implements LogFactoryInterface
{
    public function create(): Log
    {
        return new Log();
    }
}
