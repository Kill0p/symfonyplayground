<?php
namespace App\DocumentFactory;

use App\Document\Customer;

class CustomerFactory implements \App\DocumentFactory\CustomerFactoryInterface
{
    public function create(): Customer
    {
        return new Customer();
    }
}
