<?php
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 20.12.2019
 */

namespace App\DocumentFactory;

use App\Document\Log;

interface LogFactoryInterface
{
    public function create(): Log;
}
