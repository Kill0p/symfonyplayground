<?php
/**
 * Created by Maciej Strzelczyk <mstrzelczyk@media4u.pl>
 * Date: 20.12.2019
 */

namespace App\DocumentFactory;

interface CustomerFactoryInterface
{
    public function create(): \App\Document\Customer;
}
