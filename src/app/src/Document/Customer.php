<?php
namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document
 */
class Customer
{
    /**
     * @MongoDB\Id
     * @var string
     */
    private $id;
    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank
     * @Assert\Email
     * @var string
     */
    private $email;
    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    private $firstName;
    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    private $lastName;
    /**
     * @MongoDB\Field(type="date")
     * @var \DateTime
     */
    private $birthDate;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTime $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @Assert\IsTrue
     */
    public function isEmailFromMe(): bool
    {
        return $this->getEmail() === 'mstrzelczyk@media4u.pl';
    }
}
