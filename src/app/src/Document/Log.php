<?php
namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Log
{
    /**
     * @MongoDB\Id
     * @var string
     */
    private $id;
    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    private $message;
    /**
     * @MongoDB\Field(type="timestamp")
     * @var string
     */
    private $createdAt;
    /**
     * @MongoDB\Field(type="timestamp")
     * @var string
     */
    private $notifiedAt;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getNotifiedAt(): string
    {
        return $this->notifiedAt;
    }

    public function setNotifiedAt(string $notifiedAt): void
    {
        $this->notifiedAt = $notifiedAt;
    }
}
