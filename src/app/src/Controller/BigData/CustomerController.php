<?php
namespace App\Controller\BigData;

use App\Document\Customer;
use App\Form\BigData\CustomerType;
use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends \FOS\RestBundle\Controller\AbstractFOSRestController
{
    /** @var DocumentManager */
    private $mongoManager;

    public function __construct(
        DocumentManager $mongoManager
    ) {
        $this->mongoManager = $mongoManager;
    }

    /**
     * @Get("/customers")
     */
    public function getCustomersAction()
    {
        $customers = $this->mongoManager->getRepository(Customer::class)->findAll();

        $view = $this->view($customers, 200)
            ->setFormat('json');

        return $this->handleView($view);
    }

    /**
     * @Get("/customers/{id}")
     */
    public function getCustomerAction(string $id)
    {
        $customer = $this->mongoManager->getRepository(Customer::class)->find($id);

        $view = $this->view($customer)
            ->setFormat('json');

        return $this->handleView($view);
    }

    /**
     * @Post("/customers")
     */
    public function addCustomersAction(Request $request)
    {
        $content = $request->getContent();
        $data = json_decode($content, true);

        $form = $this->createForm(CustomerType::class, null, ['csrf_protection' => false]);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {

            $customer = $form->getData();
            $this->mongoManager->persist($customer);
            $this->mongoManager->flush();

            $view = $this->view($customer, 201)
                ->setFormat('json');

            return $this->handleView($view);
        } elseif ($form->isSubmitted() && !$form->isValid()) {

            $errors = $form->getErrors(true);
            $view = $this->view($errors, 422)
                ->setFormat('json');

            return $this->handleView($view);
        }
        $view = $this->view('Something went wrong', 400)
            ->setFormat('json');

        return $this->handleView($view);
    }
}
