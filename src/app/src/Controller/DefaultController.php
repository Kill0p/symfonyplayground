<?php
declare(strict_types=1);

namespace App\Controller;

use App\Document\Customer;
use Doctrine\ODM\MongoDB\DocumentManager;
use Media4u\ArchiveImporter\Importer\ManagerFactory;
use Media4u\Framework\Common\DataObject;
use Media4u\Framework\Common\DataObjectSimpleSerializer;
use Modules\Media\Entity\TestFile;
use Modules\Media\Entity\TestFileTest;
use Modules\Media\Model\FileResource;
use Modules\Media\Entity\File;
use Modules\Media\Model\TestFilesCollection;
use Modules\Media\Model\TestFilesResource;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class DefaultController extends AbstractController
{
    /** @var FileResource */
    private $fileResource;
    /** @var TestFilesResource */
    private $testFilesResource;
    /** @var TestFilesCollection */
    private $testFileCollection;
    /** @var \Modules\Customer\Model\CustomerResource */
    private $customerResource;

    public function __construct(
        FileResource $fileResource,
        TestFilesResource $testFilesResource,
        TestFilesCollection $testFileCollection,
        \Modules\Customer\Model\CustomerResource $customerResource
    )
    {
        $this->fileResource = $fileResource;
        $this->testFilesResource = $testFilesResource;
        $this->testFileCollection = $testFileCollection;
        $this->customerResource = $customerResource;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function index(
        CacheInterface $cache,
        DocumentManager $documentManager,
        ManagerFactory $managerFactory,
        DataObject $dataObject,
        DataObjectSimpleSerializer $simpleSerializer
    ): \Symfony\Component\HttpFoundation\Response
    {
        $file = new File();
        $file->setName('file_name_1');
        $file->setPath(['foo' => 'bar', 'baz' => 1, 'here' => ['there' => 'omg']]);

        $this->fileResource->save($file);


        $file2 = new File();
        $this->fileResource->load($file2, 1, 'id');
        $file2->getPath();
        $file2->getName();

        $this->testFileCollection->getSomethingSpecial();
        $this->testFileCollection->load();


//        $this->testFileCollection->setCacheKey(__FUNCTION__.'lolol');
//        $this->testFileCollection->setEntityClass(TestFileTest::class);
//        /**
//         * @var TestFileTest $testFile
//         */
//        foreach ($this->testFileCollection->getItems() as $testFile) {
//            dump($testFile);
//        }

//        $this->getUser()

//        $this->fileResource->load($file, [3]);
//        $testFile = new TestFile();
//        $this->testFilesResource->load($testFile, [1]);
//        $newTestFile = new TestFile();
//        $newTestFile->setData('id', 7);
//        $newTestFile->setData('order_id', 100);
//        $newTestFile->setData('yolo', 12102);
//        $result = $this->testFilesResource->update($newTestFile);

//        $files = new Files();
//        dump($files->setData('key','trololo'));
//        dump($simpleSerializer->toXml($files));
//        dump($simpleSerializer->toJson($files));

        $manager = $managerFactory->create(
            'media/pub/imports/product_pictures/',
            'media/pub/imports/product_pictures/catalog/',
            'media/pub/imports/product_pictures/zip/',
            '/tmp/uploaded/archive/full/path/bar.zip'
        );

        dump($manager);

        $number = $cache->get('random_number', function (ItemInterface $item) {
            $item->expiresAfter(10);

            return random_int(0, 100);
        });

//        $customer = $customerFactory->create();
//        $customer->setEmail('mstrzelczyk@media4u.pl');
//        $customer->setFirstName('Maciej');
//        $customer->setLastName('Strzelczyk');
//        $customer->setBirthDate(new \DateTime());

//        $log = $logFactory->create();
//        $log->setCreatedAt((new \DateTime())->getTimestamp());
//        $log->setMessage('Random exception message');
//        $documentManager->persist($log);
//        $documentManager->persist($customer);
//        $documentManager->flush();

        $customers = $documentManager->getRepository(Customer::class)->findAll();

        return $this->render('@Project/mainpage/index.html.twig', ['number' => $number]);
    }
}
