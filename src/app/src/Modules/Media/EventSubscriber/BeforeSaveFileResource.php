<?php
declare(strict_types=1);

namespace Modules\Media\EventSubscriber;

use Media4u\Framework\Event\ActiveRecord\BeforeSave;
use Modules\Media\Entity\File;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BeforeSaveFileResource implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            \Media4u\Framework\Event\ActiveRecord\BeforeSave::class => 'onBeforeSave',
        ];
    }

    public function onBeforeSave(BeforeSave $event): void
    {
        if (get_class($event->getEntity()) === File::class) {
            dump(File::class);
        }
    }
}
