<?php
declare(strict_types=1);

namespace Modules\Media\Model;

use Media4u\Framework\ActiveRecord\Serialization\SerializableFieldConfig;
use Modules\Media\Entity\File;

class FileResource extends \Media4u\Framework\ActiveRecord\AbstractResource
{
    protected function init(): void
    {
        $this->serializableFields = [
            'path' => new SerializableFieldConfig(
                \Media4u\Framework\ActiveRecord\Serialization\ResourceSerializerInterface::JSON,
                \Media4u\Framework\ActiveRecord\Serialization\ResourceSerializerInterface::JSON,
                false,
                null,
                null
            )
        ];
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function predefinedQuery(): File
    {
        $connection = $this->getConnection();
        $query = $connection->executeQuery("SELECT * FROM {$this->getTableName()} WHERE {$this->getPrimaryKey()} = :identity", ['identity' => 1]);
        $result = $query->fetch();

        $file = new File();
        $file->setPath($result['path']);
        $file->setName($result['name']);

        return $file;
    }
}
