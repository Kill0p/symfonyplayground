<?php
declare(strict_types=1);

namespace Modules\Media\Model;

use Media4u\Framework\ActiveRecord\AbstractCollection;
use Modules\Media\Entity\TestFileTest;

class TestFilesCollection extends AbstractCollection
{
    public function getSomethingSpecial()
    {
        $this->setCacheKey(__FUNCTION__);

        $this->getQueryBuilder()
            ->select('main_table.id as mid, main_table.path as main_table_path, main_table.order_id, ff.path')
            ->leftJoin('main_table', 'media.file','ff', 'main_table.id = ff.id');

        $this->setEntityClass(TestFileTest::class);
    }

    public function getSomethingSpecial2()
    {
        $this->setCacheKey('random_cache_key');

        $this->getQueryBuilder()
            ->select('main_table.id as mid, main_table.path as main_table_path, main_table.order_id, ff.path')
            ->leftJoin('main_table', 'media.file','ff', 'main_table.id = ff.id')
            ->rightJoin('main_table', 'media.file','ff', 'main_table.id = ff.id');

        $this->setEntityClass(TestFileTest::class);
    }
}
