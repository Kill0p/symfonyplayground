<?php
declare(strict_types=1);

namespace Modules\Media\Entity;

use Media4u\Framework\ActiveRecord\AbstractEntity;

class File extends AbstractEntity
{
    public function setName(string $name): void
    {
        $this->setData('name', $name);
    }

    public function setPath(array $path): void
    {
        $this->setData('path', $path);
    }

    public function getName(): string
    {
        return $this->getData('name');
    }

    public function getPath(): array
    {
        return $this->getData('path');
    }
}
