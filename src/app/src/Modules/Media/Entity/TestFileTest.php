<?php
declare(strict_types=1);

namespace Modules\Media\Entity;

class TestFileTest extends TestFile
{
    public function setRandomValue(string $randomValue)
    {
        $this->setData('random_value', $randomValue);
    }

    public function getRandomValue():string
    {
        return $this->getData('random_value');
    }
}
