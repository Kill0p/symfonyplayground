<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter\Controller;

class ImportPictureController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    public function index(\Media4u\ArchiveImporter\Importer\ManagerFactory $managerFactory): \Symfony\Component\HttpFoundation\Response
    {
        $path = $this->getParameter('kernel.project_dir').'/public/files/import_zip_191120-0801.zip';

        $manager = $managerFactory->create(
            'public/media/pub/imports/product_pictures/',
            'public/media/res/imports/product_pictures/catalog/',
            'public/media/res/imports/product_pictures/zip/',
            $path
        );

        $manager->import();

        return $this->render('@ArchiveImporter/mainpage/index.html.twig', ['number' => 10]);
    }
}
