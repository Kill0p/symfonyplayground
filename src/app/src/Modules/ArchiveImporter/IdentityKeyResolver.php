<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter;

use Media4u\ArchiveImporter\Exception\Interpreters\InterpreterException;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;
use Media4u\ArchiveImporter\Interpreters\ProductNameTrait;
use Media4u\ArchiveImporter\Resolvers\IdentityKeyResolverInterface;

class IdentityKeyResolver implements IdentityKeyResolverInterface
{
    /**
     * Used trait with needed methods for this kind of interpreted archive files
     * This is implementation of business abstraction which additionally uses same trait from business code
     */
    use ProductNameTrait;

    /** @var array */
    private $identifierKeys = [];

    public function acquireIdentityValuesForFiles(ArchiveFilesInterface $archiveFiles): void
    {
        /** @var ArchiveFileInterface $archiveFile */
        foreach ($archiveFiles as $key => $archiveFile) {
            try {
                $this->handleFullPictureName($archiveFile);
            } catch (InterpreterException $e) {
                $archiveFile->setIdentityKeyValue(null);
            }
            $this->identifierKeys[ $key ] = $this->pictureName;
        }
        foreach ($this->identifierKeys as $key) {

        }
    }
}
