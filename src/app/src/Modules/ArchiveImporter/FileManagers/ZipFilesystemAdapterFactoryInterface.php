<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter\FileManagers;

interface ZipFilesystemAdapterFactoryInterface
{
    public function create(string $location): \League\Flysystem\FilesystemInterface;
}
