<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter\FileManagers;

use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;

class ZipFilesystemAdapterFactory implements ZipFilesystemAdapterFactoryInterface
{
    public function create(string $location): \League\Flysystem\FilesystemInterface
    {
        $zipArchiveAdapter = new ZipArchiveAdapter($location);

        return new Filesystem($zipArchiveAdapter);
    }
}
