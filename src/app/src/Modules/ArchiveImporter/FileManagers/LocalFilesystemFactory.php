<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter\FileManagers;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;

class LocalFilesystemFactory implements LocalFilesystemFactoryInterface
{
    /** @var string */
    private $rootDirectoryPath;

    public function __construct(
        string $rootDirectoryPath
    ) {
        $this->rootDirectoryPath = $rootDirectoryPath;
    }

    public function create(string $directoryName = ''): FilesystemInterface
    {
        if (empty($directoryName)) {
            $directoryName = $this->rootDirectoryPath;
        }

        return new Filesystem(new \League\Flysystem\Adapter\Local($directoryName));
    }
}
