<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter\FileManagers;

use League\Flysystem\FilesystemInterface;

interface LocalFilesystemFactoryInterface
{
    public function create(string $directoryName = ''): FilesystemInterface;
}
