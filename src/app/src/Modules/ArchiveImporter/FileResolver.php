<?php
declare(strict_types=1);

namespace Modules\ArchiveImporter;

use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFile;
use Media4u\ArchiveImporter\Importer\ArchiveFileInterface;
use Media4u\ArchiveImporter\Importer\ArchiveFiles;
use Media4u\ArchiveImporter\Importer\ArchiveFilesInterface;
use Media4u\ArchiveImporter\Segregators\FileInterface;
use Modules\ArchiveImporter\FileManagers\LocalFilesystemFactoryInterface;
use Modules\ArchiveImporter\FileManagers\ZipFilesystemAdapterFactoryInterface;
use Media4u\ArchiveImporter\Exception\FileResolvers\FileResolverException;

/**
 * Mediator pattern
 * It connects different services to compute some information, those services does not know about each other.
 * Just passing data objects from one service to another.
 * Bridge pattern
 * Finally $this class is implementation of abstraction which is used in @see \Media4u\ArchiveImporter\Importer\Manager
 * Manager cares about results from bridge class which logic wraps parts of logic of computing things.
 *
 * @author Maciej Strzelczyk <m.strz3lczyk@gmail.com>
 */
class FileResolver implements \Media4u\ArchiveImporter\Resolvers\FileResolverInterface
{
    /** @var ZipFilesystemAdapterFactoryInterface */
    private $zipFilesystemAdapterFactory;
    /** @var FilesystemInterface */
    private $zipFilesystem;
    /** @var LocalFilesystemFactoryInterface */
    private $localFilesystemFactory;

    public function __construct(
        ZipFilesystemAdapterFactoryInterface $zipFilesystemAdapterFactory,
        LocalFilesystemFactoryInterface $localFilesystemFactory
    ) {
        $this->zipFilesystemAdapterFactory = $zipFilesystemAdapterFactory;
        $this->localFilesystemFactory = $localFilesystemFactory;
    }

    /**
     * @inheritDoc
     */
    public function listContent(string $directoryName): \Media4u\ArchiveImporter\Segregators\FilesInterface
    {
        $localFilesystem = $this->localFilesystemFactory->create('/');
        $content = $localFilesystem->listContents($directoryName);

        if (empty($content)) {
            throw new FileResolverException('Could not list content of directory '.$directoryName);
        }

        $files = new \Media4u\ArchiveImporter\Segregators\Files();

        foreach ($content as $item) {
            $file = new \Media4u\ArchiveImporter\Segregators\File();
            $file->setFullPath(DIRECTORY_SEPARATOR.$item['path']);
            if ($item['type'] === 'dir') {
                $itemType = FileInterface::TYPE_DIRECTORY;
            } else if ($item['type' === 'file']) {
                $itemType = FileInterface::TYPE_FILE;
            } else {
                throw new FileResolverException('Could not specified content item type');
            }
            $file->setType($itemType);

            $files[] = $file;
        }

        return $files;
    }

    public function deleteDirectory(string $directoryName): void
    {
        $localFilesystem = $this->localFilesystemFactory->create('/');
        $isDeletedDir = $localFilesystem->deleteDir($directoryName);

        if ($isDeletedDir === false) {
            throw new  FileResolverException('Could not delete directory at path '.$directoryName);
        }
    }

    public function deleteFile(string $fullPath): void
    {
        $localFilesystem = $this->localFilesystemFactory->create('/');
        $isDeletedDir = $localFilesystem->deleteDir($fullPath);

        if ($isDeletedDir === false) {
            throw new FileResolverException('Could not delete file at path '.$fullPath);
        }
    }

    public function copyFile(string $fromFullPath, string $toFullPath): void
    {
        return;
    }

    /**
     * @inheritDoc
     */
    public function moveArchive(string $archiveFileUploadPath, string $getNewArchiveFileName, string $newArchiveFileFullPath): void
    {
        $localFilesystem = $this->localFilesystemFactory->create('/');
        $isCopiedSuccessfully = $localFilesystem->rename($archiveFileUploadPath, $newArchiveFileFullPath);

        if ($isCopiedSuccessfully === false) {
            throw new FileResolverException('Could not move archive file, from '.$archiveFileUploadPath.' to '.$newArchiveFileFullPath);
        }

        //todo modyfikacja mediów dla nowej sciezki.
    }

    /**
     * @inheritDoc
     */
    public function handlePictures(string $targetDirectoryNameForArchiveFile, array $arrangedPicturePaths): void
    {
        // TODO: Implement handlePictures() method.
    }

    /**
     * @inheritDoc
     */
    public function getListOfFilesFromArchive(string $fullPathToArchive): ArchiveFilesInterface
    {
        $this->zipFilesystem = $this->zipFilesystemAdapterFactory->create($fullPathToArchive);
        $archiveContent = $this->zipFilesystem->listContents();

        $archiveFiles = new ArchiveFiles();//todo abstrakcja na to?
        $this->flatMultiDimensionalList($archiveContent, $archiveFiles);

        return $archiveFiles;
    }

    private function flatMultiDimensionalList(array $list, ArchiveFiles $files): void
    {
        foreach ($list as $item) {
            if ($item['type'] === 'dir') {
                $dirFiles = $this->zipFilesystem->listContents($item['path']);
                $this->flatMultiDimensionalList($dirFiles, $files);
            } else {
                $files[] = new ArchiveFile($item['basename'], $item['extension'], $item['path']);//todo abstrakcja na to? czy to sa napewno poprawne sciezki
            }
        }
    }

    /**
     * @throws FileResolverException
     */
    public function copyFromArchiveToFile(string $archiveFileUploadPath, ArchiveFileInterface $file): void
    {
        $this->zipFilesystem = $this->zipFilesystemAdapterFactory->create($archiveFileUploadPath);

        try {
            $content = $this->zipFilesystem->read($file->getFullPath());
        } catch (FileNotFoundException $e) {
            throw new FileResolverException('Could not find archive file for path '.$file->getFullPath());
        }

        if (empty($content)) {
            throw new FileResolverException('Could not read content of archive file '.$file->getFullPath());
        }

        $localFilesystem = $this->localFilesystemFactory->create($file->getSavePathDirectoryName());
        $isPutSuccessfully = $localFilesystem->put($file->getFullPath(), $content);

        if ($isPutSuccessfully === false) {
            throw new FileResolverException('Could not save file '.$file->getFullPath().' in local filesystem '.$file->getSavePathDirectoryName());
        }

        //todo umieszczenie sciezek do bazy zapisanych plikow
    }
}
