<?php
declare(strict_types=1);

namespace Modules\Customer\Controller;

use Modules\Customer\Security\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function register(Request $request)
    {
        $customer = new Customer();
        $customer->setPassword($this->passwordEncoder->encodePassword($customer, 'dupa'));
    }

    public function logout(): void
    {

    }
}
