<?php
declare(strict_types=1);

namespace Modules\Customer\Entity;

class Customer extends \Media4u\Framework\ActiveRecord\AbstractEntity
{
    public function getId(): int
    {
        return $this->getData('id');
    }

    public function setId(int $id): void
    {
        $this->setData('id', $id);
    }

    public function getEmail(): string
    {
        return $this->getData('email');
    }

    public function setEmail(string $email): void
    {
        $this->setData('email', $email);
    }

    public function getPassword(): string
    {
        return $this->getData('password');
    }

    public function setPassword(string $password): void
    {
        $this->setData('password', $password);
    }

    public function getRoles(): array
    {
        return $this->getData('roles');
    }

    public function setRoles(array $roles): void
    {
        $this->setData('roles', $roles);
    }
}
