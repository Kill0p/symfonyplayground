<?php
declare(strict_types=1);

namespace Modules\Customer\Security;

use Modules\Customer\Model\CustomerResource;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CustomerProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    /** @var CustomerResource */
    private $customerResource;

    public function __construct(
        CustomerResource $customerResource
    ) {
        $this->customerResource = $customerResource;
    }

    /**
     * Symfony calls this method if you use features like switch_user
     * or remember_me.
     *
     * If you're not using these features, you do not need to implement
     * this method.
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        // Load a Customer object from your data source or throw UsernameNotFoundException.
        // The $username argument may not actually be a username:
        // it is whatever value is being returned by the getUsername()
        // method in your Customer class.
        throw new \Exception('TODO: fill in loadUserByUsername() inside '.__FILE__);
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * Customer object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh Customer data.
     *
     * If your firewall is "stateless: true" (for a pure API), this
     * method is not called.
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof Customer) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $customer = new \Modules\Customer\Entity\Customer();
        $this->customerResource->load($customer, $user->getUsername(), 'email');

        if ($customer->isEmpty()) {
            $exception = new UsernameNotFoundException();
            $exception->setUsername($user->getEmail());

            throw $exception;
        }

        $user->setEmail($customer->getEmail());
        $user->setId($customer->getId());
        $user->setPassword($customer->getPassword());
        $user->setRoles($customer->getRoles());

        return $user;
    }

    /**
     * Tells Symfony to use this provider for this Customer class.
     */
    public function supportsClass($class)
    {
        return Customer::class === $class;
    }

    /**
     * Upgrades the encoded password of a user, typically for using a better hash algorithm.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        // TODO: when encoded passwords are in use, this method should:
        // 1. persist the new password in the user storage
        // 2. update the $user object with $user->setPassword($newEncodedPassword);
    }
}
