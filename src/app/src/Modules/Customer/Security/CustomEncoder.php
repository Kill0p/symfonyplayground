<?php
declare(strict_types=1);

namespace Modules\Customer\Security;

class CustomEncoder extends \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder
{
}
