let greeting = function(name) {

    return `Hello, ${name} !!!`
};

document.addEventListener('DOMContentLoaded', function() {
    let mainTitle = document.querySelector('#main-title');
    let randomNumber = mainTitle.dataset.randomNumber;

    mainTitle.textContent = greeting(randomNumber);
});