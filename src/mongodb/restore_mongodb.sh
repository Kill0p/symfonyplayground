#!/bin/bash
mongorestore -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD" --gzip --archive=/docker-entrypoint-initdb.d/mongodb_dump.gz