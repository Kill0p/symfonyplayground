#!/usr/bin/env bash
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
WORK_DIR=$(dirname "$SCRIPT_DIR")

DUMP_NAME=mongodb_dump.gz

docker exec symfonyplayground_db_mongo /bin/bash -c 'mongodump -u ${MONGO_INITDB_ROOT_USERNAME} -p ${MONGO_INITDB_ROOT_PASSWORD} --gzip --archive=/root/'$DUMP_NAME
docker cp symfonyplayground_db_mongo:/root/mongodb_dump.gz "$WORK_DIR/src/mongodb/$DUMP_NAME"
chmod a+x "$WORK_DIR/src/mongodb/$DUMP_NAME"