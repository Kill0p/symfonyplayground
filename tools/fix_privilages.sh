#!/usr/bin/env bash
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
WORK_DIR=$(dirname "$SCRIPT_DIR")

APP_DIR="$WORK_DIR/src/app"

sudo chown -R "$(id -u)":www-data "$APP_DIR"
sudo chmod ug+w -R "$APP_DIR/public"
sudo chmod ug+x "$APP_DIR/bin/console"