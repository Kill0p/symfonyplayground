#!/usr/bin/env bash
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
WORK_DIR=$(dirname "$SCRIPT_DIR")

DUMP_NAME=dump.sql.gz

docker exec -t symfonyplayground_db_postgres /bin/bash -c 'pg_dump -U ${POSTGRES_USER} ${POSTGRES_DB} | gzip > ~/'$DUMP_NAME
docker cp symfonyplayground_db_postgres:/root/dump.sql.gz "$WORK_DIR/src/db/$DUMP_NAME"
chmod a+x "$WORK_DIR/src/db/$DUMP_NAME"